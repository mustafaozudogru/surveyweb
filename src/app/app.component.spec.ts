import { ComponentFixture, TestBed, waitForAsync } from '@angular/core/testing';
import { AppComponent } from './app.component';

let app: AppComponent;
let fixture: ComponentFixture<AppComponent>;

describe('AppComponent', () => {

  beforeEach(waitForAsync(async () => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent
      ],
    }).compileComponents();

    await createComponent();
  }));

  it('should create the app', async () => {
    expect(app).toBeTruthy();
  });

  it('should have as title \'app\'', async () => {
    expect(app.title).toContain('Survey');
  });

  it('should contain as class \'container\'', async () => {
    const element = fixture.debugElement.nativeElement.querySelector('div');
    expect(element.getAttribute('class')).toContain('container');
  });

  // Helper method.
  async function createComponent() {
    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;

    // 1st change detection triggers ngOnInit which gets a survey
    fixture.detectChanges();
    await fixture.whenStable();
    // 2nd change detection displays the async-fetched survey
    fixture.detectChanges();
  }
});
