import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Survey } from '../models';
import { asyncData, asyncError } from '../tests/test-helpers';
import { getTestSurvey } from '../tests/test-survey';
import { SurveyService } from '../services/survey.service'
import { SurveyPostResult } from '../models/surveyPostResult';

describe('HttpClient testing', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let getTestUrl = 'http://localhost:26324/v1/survey/by-name-with-ip-control?name=Survey';
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
    });

    // Inject the http service and test controller for each test
    httpClient = TestBed.inject(HttpClient);
    httpTestingController = TestBed.inject(HttpTestingController);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });


  it('should test HttpClient.get', () => {
    const testData: Survey = getTestSurvey();;

    // Make an HTTP GET request
    httpClient.get<Survey>(getTestUrl)
      .subscribe(data =>
        // When observable resolves, result should match test data
        expect(data).toEqual(testData)
      );

    // The following `expectOne()` will match the request's URL.
    // If no requests or multiple requests matched that URL
    // `expectOne()` would throw.
    const req = httpTestingController.expectOne(getTestUrl);

    // Assert that the request is a GET.
    expect(req.request.method).toEqual('GET');

    // Respond with mock data, causing Observable to resolve.
    // Subscribe callback asserts that correct data was returned.
    req.flush(testData);

    // Finally, assert that there are no outstanding requests.
    httpTestingController.verify();
  });

  it('should test for 404 error', () => {
    const emsg = 'deliberate 404 error';

    httpClient.get<Survey>(getTestUrl).subscribe(
      data => fail('should have failed with the 404 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(404, 'status');
        expect(error.error).toEqual(emsg, 404);
      }
    );

    const req = httpTestingController.expectOne(getTestUrl);

    // Respond with mock error
    req.flush(emsg, { status: 404, statusText: 'Not Found' });
  });

  it('should test for 500 error', () => {
    const emsg = 'deliberate 500 error';

    httpClient.get<Survey>(getTestUrl).subscribe(
      data => fail('should have failed with the 500 error'),
      (error: HttpErrorResponse) => {
        expect(error.status).toEqual(500, 'status');
        expect(error.error).toEqual(emsg, 500);
      }
    );

    const req = httpTestingController.expectOne(getTestUrl);

    // Respond with mock error
    req.flush(emsg, { status: 500, statusText: 'Already answered' });
  });

  it('should test for network error', () => {
    const emsg = 'simulated network error';

    httpClient.get<Survey>(getTestUrl).subscribe(
      data => fail('should have failed with the network error'),
      (error: HttpErrorResponse) => {
        expect(error.error.message).toEqual(emsg, 'message');
      }
    );

    const req = httpTestingController.expectOne(getTestUrl);

    // Create mock ErrorEvent, raised when something goes wrong at the network level.
    // Connection timeout, DNS error, offline, etc
    const mockError = new ErrorEvent('Network error', {
      message: emsg,
    });

    // Respond with mock error
    req.error(mockError);
  });
});

describe('SurveyService (with spies)', () => {
  let httpClientSpy: { get: jasmine.Spy, post: jasmine.Spy };
  let routerSpy: { get: jasmine.Spy, post: jasmine.Spy };
  let surveyService: SurveyService;
  let expectedSurvey: Survey;

  beforeEach(() => {
    httpClientSpy = jasmine.createSpyObj('HttpClient', ['get', 'post']);
    routerSpy = jasmine.createSpyObj('Router', ['navigate']);
    surveyService = new SurveyService(httpClientSpy as any, routerSpy as any);
    expectedSurvey = getTestSurvey();
  });

  it('should \'get\' return 404 survey (HttpClient called once)', (done: DoneFn) => {

    const errorResponse = new HttpErrorResponse({
      error: { message: "Not Found", messageCode: 404, isSuccessful: false, data: null },
      status: 404, statusText: 'Not Found'
    });

    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    surveyService.get().subscribe(
      survey => done.fail(errorResponse),
      error => {
        expect(error.error.messageCode).toBe(404);
        expect(error.error.message).toContain('Not Found');
        expect(error.error.isSuccessful).toBe(false);
        expect(error.error.data).toBe(null);
        done();
      }
    );

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should \'get\' return 500 survey (HttpClient called once)', (done: DoneFn) => {

    const errorResponse = new HttpErrorResponse({
      error: { message: "Already added", messageCode: 500, isSuccessful: false, data: null },
      status: 500, statusText: 'Already added'
    });

    httpClientSpy.get.and.returnValue(asyncError(errorResponse));

    surveyService.get().subscribe(
      survey => done.fail(errorResponse),
      error => {
        expect(error.error.messageCode).toBe(500);
        expect(error.error.message).toContain('Already added');
        expect(error.error.isSuccessful).toBe(false);
        expect(error.error.data).toBe(null);
        done();
      }
    );

    expect(httpClientSpy.get.calls.count()).toBe(1, 'one call');
  });

  it('should \'post\' return expected survey (HttpClient called once)', (done: DoneFn) => {

    let tempArr: any = [];
    expectedSurvey.reviewQuestions = expectedSurvey.questions.filter((q: any) => q.options.find((y: any) => y.selected == true) || q.value != "");

    expectedSurvey.reviewQuestions.forEach((x: any) => tempArr.push({ 'questionId': x.id, 'value': x.value, 'options': x.options.filter((q: any) => q.selected == true) }));

    let answers: SurveyPostResult = new SurveyPostResult({ ResultJson: "" });
    answers.ResultJson = JSON.stringify(tempArr);

    httpClientSpy.post.and.returnValue(asyncData(answers));

    httpClientSpy.post().subscribe(
      (x: any) => {
        expect(x).toEqual(answers);
        done();
      }
    );

    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });

  it('should \'post\' return 302 already added (HttpClient called once)', (done: DoneFn) => {
    const errorResponse = new HttpErrorResponse({
      error: { message: "Already added", messageCode: 302, isSuccessful: false, data: null },
      status: 302, statusText: 'Error'
    });

    httpClientSpy.post.and.returnValue(asyncError(errorResponse));

    surveyService.post(asyncData(new SurveyPostResult({ ResultJson: "" }))).subscribe(
      survey => done.fail(errorResponse),
      error => {
        expect(error.error.messageCode).toBe(302);
        expect(error.error.message).toContain('Already added');
        expect(error.error.isSuccessful).toBe(false);
        expect(error.error.data).toBe(null);
        done();
      }
    );

    expect(httpClientSpy.post.calls.count()).toBe(1, 'one call');
  });
});

