import { HttpClient, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { Survey } from '../models';
import { catchError, map } from 'rxjs/operators';
import { Router } from '@angular/router';

@Injectable()
export class SurveyService {
  readonly surveyApiUrl = 'http://localhost:26324/';
  constructor(private http: HttpClient, private router: Router) { }
  public str: any;

  get(): Observable<Survey> {
    const url = `${this.surveyApiUrl}v1/survey/by-name-with-ip-control?name=Survey`;

    return this.http.get<any>(url).pipe(
      map(
        (results: any) =>
          new Survey(JSON.parse(results[0].surveyJson))
      ),
      catchError((error: HttpErrorResponse) => {
        if (error && error.error && error.error.messageCode === 500) {
          this.router.navigate(['/answered']);
        } else {
          this.router.navigate(['/error']);
        }

        return throwError(error);
      })
    );
  }

  post(result: any): Observable<any> {
    const url = `${this.surveyApiUrl}v1/survey/result`;
    const httpOptions = {
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    }

    return this.http.post<any>(url, result, httpOptions).pipe(
      catchError((error: HttpErrorResponse) => {
        if (error && error.error && error.error.messageCode === 302) {
          this.router.navigate(['/error']);
        }        

        return throwError(error);
      }));
  }
}
