import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AlreadyAnsweredComponent } from './error/alreadyAnswered.component';
import { ErrorComponent } from './error/error.component';
import { ResultComponent } from './result/result.component';
import { SurveyComponent } from './survey/survey.component';

export const routes: Routes = [
  { path: '',  component: SurveyComponent},
  { path: 'result', component: ResultComponent },
  { path: 'answered', component: AlreadyAnsweredComponent },
  { path: 'error', component: ErrorComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
