import { Component, OnInit } from '@angular/core';
import { SurveyService } from '../services/survey.service';
import { Option, Question, Survey, SurveyConfig } from '../models/index';
import { Router } from '@angular/router';
import { SurveyPostResult } from '../models/surveyPostResult';

@Component({
  selector: 'app-survey',
  templateUrl: './survey.component.html',
  styleUrls: ['./survey.component.css'],
  providers: [SurveyService]
})
export class SurveyComponent implements OnInit {
  surveys!: any[];
  survey: Survey = new Survey(null);
  surveyId: any;
  mode!: string;
  surveyName!: string;
  config: SurveyConfig = {
    'allowBack': true
  };
  answers: SurveyPostResult = new SurveyPostResult({ ResultJson: "" });

  pager = {
    index: 0,
    size: 1,
    count: 1
  };

  constructor(private surveyService: SurveyService, private router: Router) { }

  ngOnInit() {
    this.surveyService.get().subscribe(res => {
      this.survey = new Survey(res);
      this.pager.count = this.survey.questions.length;
    });

    this.mode = 'survey';
  }

  surveyQuestions() {
    return (this.survey.questions) ?
      this.survey.questions.slice(this.pager.index, this.pager.index + this.pager.size) : [];
  }

  onSelect(question: Question, optionId: number) {
    if (question.questionTypeId == 1 && question.multiSelect == false) {
      question.options.forEach((x: Option) => { x.id !== optionId ? x.selected = false : x.selected = true; });
    }
  }

  cleanQuestionAnswers(question: Question) {
    if (question.questionTypeId == 1) {
      question.options.forEach((x: Option) => { x.selected = false; });
    } else if (question.questionTypeId == 2) {
      question.value = '';
    }
  }

  previous(index: number) {
    this.cleanQuestionAnswers(this.survey.questions[index + 1]);

    let counter = 0;

    // The previous questions are checked from the given index to understand which question is correct to jump to.
    for (let i = index; i >= 0; i--) {
      let question = this.survey.questions[i];
      let response = this.parentOperation(question);

      // If response == false, counter is increased and skipped to previous question.
      if (!response) {
        counter++;
      } else {
        break;
      }
    }

    // Provided to skip according to counter.
    index = counter != 0 ? index - counter : index;

    if (index >= 0 && index < this.pager.index) {
      this.pager.index = index;
    }
  }

  next(index: number) {
    let counter = 0;

    // The next questions are checked from the given index to understand which question is correct to jump to.
    for (let i = index; i < this.survey.questions.length; i++) {
      let question = this.survey.questions[i];
      let response = this.parentOperation(question);

      if (response) {
        break;
      }

      counter++;
    }

    // Check if the question has been answered.
    let isAnswered = this.isAnswered(this.survey.questions[this.pager.index]);

    // Provided to skip according to counter.
    index = counter != 0 ? index + counter : index;

    if (index >= 0 && index < this.pager.count && isAnswered && index > this.pager.index) {
      this.pager.index = index;
    }
  }

  parentOperation(question: Question) {
    // if question doesn't have a parent.
    if (!question.parent || !question.parent.optionId) {
      return true;
    }

    // if question has a parent. 
    // 1) Parent question is retrieved by id.
    // 2) Selected option of parent question is retrieved.
    // 3) The option id in the question with the parameter is compared with the selected option of the parent question.
    var parentQuestions = this.survey.questions.find(x => x.id == question.parent.parentId);
    var selectedOption = parentQuestions ? parentQuestions.options.find(x => x.selected == true)?.id : null;

    return (selectedOption === question.parent.optionId);
  };

  isAnswered(question: Question) {
    if (question.questionTypeId == 1 && question.options.find((x: Option) => x.selected)) {
      return true;
    } else if (question.questionTypeId == 2) {
      return question.value !== "";
    }

    return false;
  };

  modeOperations(mode: string) {
    if (mode === 'review') {
      this.survey.reviewQuestions = (this.survey.questions && this.survey.questions.length > 0) ? this.survey.questions.filter((q: any) => q.options.find((y: any) => y.selected == true) || q.value != "") : [];
    }

    this.mode = mode;
  };

  canReview(index: number) {
    return (index == this.pager.count - 1 && this.survey.questions && this.isAnswered(this.survey.questions[index]));
  };

  onSubmit() {
    let tempArr: any = [];
    this.survey.reviewQuestions.forEach((x: any) => tempArr.push({ 'questionId': x.id, 'value': x.value, 'options': x.options.filter((q: any) => q.selected == true) }));
    this.answers.ResultJson = JSON.stringify(tempArr);

    this.surveyService.post(this.answers).subscribe(() => this.router.navigate(['/result']));
  }
}
