import { ComponentFixture, TestBed } from '@angular/core/testing';
import { SurveyComponent } from './survey.component';
import { By } from '@angular/platform-browser';
import { SurveyService } from '../services/survey.service';
import { DebugElement } from '@angular/core';
import { Survey } from '../models';
import { getTestSurvey } from '../tests/test-survey';
import { asyncData } from '../tests/test-helpers';
import { RouterTestingModule } from '@angular/router/testing';
import { SurveyPostResult } from '../models/surveyPostResult';

let component: SurveyComponent;
let fixture: ComponentFixture<SurveyComponent>;
let surveyDe: DebugElement;

describe('SurveyComponent', () => {
  describe('when override the SurveyService provided', overrideSetup);
});

function overrideSetup() {
  class SurveyServiceSpy {
    testSurvey: Survey = getTestSurvey();
    testAnswers: SurveyPostResult = new SurveyPostResult({ ResultJson: "[{\"questionId\":1010,\"value\":\"\",\"options\":[{\"id\":1055,\"name\":\"Yes\",\"selected\":true}]}]" });

    get = jasmine.createSpy('get').and.callFake(
      () => asyncData(Object.assign({}, this.testSurvey)));
    post = jasmine.createSpy('post').and.callFake(
      () => asyncData(Object.assign({}, this.testAnswers)));
  }

  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        providers: [
          { provide: SurveyService, useValue: {} }
        ],
        imports: [RouterTestingModule]
      })
      .overrideComponent(
        SurveyComponent,
        { set: { providers: [{ provide: SurveyService, useClass: SurveyServiceSpy }] } })
      .compileComponents();
  });

  let spy: SurveyServiceSpy;

  beforeEach(async () => {
    await createComponent();
    // get the component's injected SurveyServiceSpy
    spy = fixture.debugElement.injector.get(SurveyService) as any;
  });

  // TEMPLATE LOGIC

  it('should be equal \'surveyName\' element with \'component.survey.name\'', () => {
    expect(surveyDe.query(By.css('#surveyName')).nativeElement.innerText).toContain(component.survey.name);
  });

  it('should be equal \'surveyDesc\' element with \'component.survey.desciption\'', () => {
    expect(surveyDe.nativeElement.querySelector('#surveyDesc').textContent).toContain(component.survey.description);
  });

  it('should be default component mode \'survey\'', () => {
    let defaultMode = 'survey';
    expect(component.mode).toContain(defaultMode);
  });

  // FUNCTION LOGIC

  it('should \'get\' have been called once', () => {
    expect(spy.get.calls.count()).toBe(1, 'get called once');
  });

  it('should \'ngOnInit\' have been called once', () => {
    component.ngOnInit();
    expect(component.ngOnInit.call.length).toBe(1, 'get called once');
  });

  it('should \'ngOnInit\' give not empty survey object', () => {
    component.ngOnInit();
    expect(component.survey.id).toBeTruthy();
  });

  it('should \'ngOnInit\' give mode \'survey\'', () => {
    component.mode = 'test';
    component.ngOnInit();
    expect(component.mode).not.toEqual('test');
  });

  it('should \'ngOnInit\' set pager count as number of questions', () => {
    const questionCount = component.survey.questions.length;
    component.ngOnInit();
    expect(questionCount).toEqual(component.pager.count);
  });

  it('should \'surveyQuestions\' have been called once', () => {
    component.surveyQuestions();
    expect(component.surveyQuestions.call.length).toBe(1, 'get called once');
  });

  it('should \'surveyQuestions\' return a filtered question result when \'survey.questions.length\' greater than 0', () => {
    expect(component.survey.questions.length).not.toBeLessThan(1, 'questions are not empty');
    const filteredQuestionResult = component.surveyQuestions();
    expect(filteredQuestionResult.length).not.toBeLessThan(1, 'questions are not empty');
  });

  it('should \'surveyQuestions\' return empty array when \'survey.questions\' length equals 0', () => {
    component.survey.questions = [];
    expect(component.survey.questions.length).toBe(0, 'questions are empty');

    const filteredQuestionResult = component.surveyQuestions();
    expect(filteredQuestionResult.length).toBe(0, 'questions are empty');
  });

  it('should \'onSelect\' have been called once', () => {
    component.onSelect(component.survey.questions[0], 0);
    expect(component.onSelect.call.length).toBe(1, 'get called once');
  });

  it('should \'onSelect\' not work if \'questionTypeId\' is not equal to 1', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId == 2);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    component.onSelect(filteredQuestions[0], 0);
    expect(filteredQuestions[0].options.filter(x => x.selected == true).length).toBe(0, 'no selected option');
  });

  it('should \'onSelect\' not work if question multiselect option is true', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.multiSelect == true);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    component.onSelect(filteredQuestions[0], 0);
    expect(filteredQuestions[0].options.filter(x => x.selected == true).length).toBe(0, 'no selected option');
  });

  it('should \'onSelect\' work only if question multiselect option is false and \'questionTypeId\' is equal to 1', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.multiSelect === false && x.questionTypeId === 1);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    let optionId = filteredQuestions[0].options[0].id;
    expect(optionId).toBeTruthy();

    component.onSelect(filteredQuestions[0], optionId);
    expect(filteredQuestions[0].options.filter(x => x.selected == true).length).toBe(1, 'one selected option');
  });

  it('should \'cleanQuestionAnswers\' have been called once', () => {
    const filteredQuestions = component.survey.questions.filter(x => x.questionTypeId == 2);
    component.cleanQuestionAnswers(filteredQuestions[0]);
    expect(component.cleanQuestionAnswers.call.length).toBe(1, 'get called once');
  });

  it('should \'cleanQuestionAnswers\' clean selected answer if \'questiontypeId\' is equal to 1', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId === 1);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    filteredQuestions[0].options[0].selected = true;
    expect(filteredQuestions[0].options.filter(x => x.selected == true).length).toBe(1, 'one selected option');

    component.cleanQuestionAnswers(filteredQuestions[0]);
    expect(filteredQuestions[0].options.filter(x => x.selected == true).length).toBe(0, 'selected options are removed');
  });

  it('should \'cleanQuestionAnswers\' clean value if \'questiontypeId\' is equal to 2', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId === 2);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    filteredQuestions[0].value = 'Test';
    expect(filteredQuestions[0].value).toContain('Test');

    component.cleanQuestionAnswers(filteredQuestions[0]);
    expect(filteredQuestions[0].value).toEqual('');
  });

  it('should \'parentOperation\' have been called once', () => {
    component.parentOperation(component.survey.questions[0]);
    expect(component.parentOperation.call.length).toBe(1, 'get called once');
  });

  it('should \'parentOperation\' return true when there is no parent in the question', () => {
    let filteredQuestions = component.survey.questions.filter(x => !x.parent || !x.parent.optionId);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    const parentRetVal = component.parentOperation(filteredQuestions[0]);
    expect(parentRetVal).toEqual(true);
  });

  it('should \'parentOperation\' return false when parent of question doesn\'t have selected item', () => {
    // Questions are retrieved which have parent.
    let filteredQuestions = component.survey.questions.filter(x => x.parent && x.parent.optionId && x.parent.parentId);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    // Question is retrieved by parent id.
    const parentQuestion = component.survey.questions.filter(x => x.id == filteredQuestions[0].parent.parentId);
    expect(parentQuestion.length).toEqual(1, 'question has a parent');

    const parentRetVal = component.parentOperation(filteredQuestions[0]);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'parentOperation\' return false when selected option is not equal to the question\'s parent option \'id\'', () => {
    // Questions are filtered which have parent.
    let filteredQuestions = component.survey.questions.filter(x => x.parent && x.parent.optionId);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    // Question is retrieved by parent id.
    const parentQuestion = component.survey.questions.filter(x => x.id == filteredQuestions[0].parent.parentId);
    expect(parentQuestion.length).toEqual(1, 'question has a parent');

    let options = filteredQuestions[0].options.filter(x => x.id !== filteredQuestions[0].parent.optionId);
    expect(options.length).toBeGreaterThan(0, 'filtered question options count is expected more than zero');

    // Option is selected.
    filteredQuestions[0].options[0].selected = true;

    const parentRetVal = component.parentOperation(filteredQuestions[0]);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'parentOperation\' return true when selected option is equal to the question\' parent option \'id\'', () => {
    // Questions are filtered which have parent.
    let filteredQuestions = component.survey.questions.filter(x => x.parent && x.parent.optionId);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    // Question is retrieved by parent id.
    const parentQuestion = component.survey.questions.filter(x => x.id == filteredQuestions[0].parent.parentId);
    expect(parentQuestion.length).toEqual(1, 'question has a parent');

    // Option is selected.
    filteredQuestions[0].options.filter(x => x.id == filteredQuestions[0].parent.optionId)[0].selected = true;

    const parentRetVal = component.parentOperation(filteredQuestions[0]);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'isAnswered\' have been called once', () => {
    component.isAnswered(component.survey.questions[0]);
    expect(component.isAnswered.call.length).toBe(1, 'get called once');
  });

  it('should \'isAnswered\' return true when \'questionTypeId\' is equal to 1 and question has selected option', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId == 1);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    // Option is selected.
    filteredQuestions[0].options[0].selected = true;

    const parentRetVal = component.isAnswered(filteredQuestions[0]);
    expect(parentRetVal).toEqual(true);
  });

  it('should \'isAnswered\' return true when \'questionTypeId\' is equal to 2 and question value is not empty', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId == 2);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    filteredQuestions[0].value = 'Test';
    const parentRetVal = component.isAnswered(filteredQuestions[0]);
    expect(parentRetVal).toEqual(true);
  });

  it('should \'isAnswered\' return false when \'questionTypeId\' is equal to 2 and selected value of question is empty', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId == 2);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    const parentRetVal = component.isAnswered(filteredQuestions[0]);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'isAnswered\' return false when \'questionTypeId\' is not equal to 1 or 2', () => {
    let filteredQuestions = component.survey.questions.filter(x => x.questionTypeId != 1 && x.questionTypeId != 2);
    expect(filteredQuestions.length).toBeGreaterThan(0, 'filtered question list is not empty');

    const parentRetVal = component.isAnswered(filteredQuestions[0]);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'modeOperations\' have been called once', () => {
    component.modeOperations('review');
    expect(component.modeOperations.call.length).toBe(1, 'get called once');
  });

  it('should \'modeOperations\' change the current mode with given mode option', () => {
    const oldMode = component.mode;
    component.modeOperations('review');

    expect(oldMode).not.toEqual(component.mode);
  });

  it('should \'modeOperations\' get the answered questions if mode is equal to \'review\' and set it to \'this.survey.reviewQuestions\'', () => {
    const oldReviewQuestionsLength = component.survey.reviewQuestions.length;

    component.pager.index = 0;
    let questionToBeAnswered = component.survey.questions[component.pager.index];
    questionToBeAnswered.options[component.pager.index].selected = true;

    component.modeOperations('review');

    expect(oldReviewQuestionsLength).not.toEqual(component.survey.reviewQuestions.length);
  });

  it('should \'canReview\' have been called once', () => {
    component.canReview(component.survey.questions.length - 1);
    expect(component.canReview.call.length).toBe(1, 'get called once');
  });

  it('should \'canReview \'return false when index is not the last page', () => {
    const questionLength = component.survey.questions.length;
    expect(questionLength).toBeGreaterThan(0, 'question list is not empty');

    const parentRetVal = component.canReview(questionLength - 1);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'canReview\' return false when question is not answered', () => {
    expect(component.survey.questions.length).toBeGreaterThan(0, 'question list is not empty');

    component.pager.index = 0;
    const parentRetVal = component.canReview(component.pager.index);
    expect(parentRetVal).toEqual(false);
  });

  it('should \'canReview\' return true when question is answered and index is the last page', () => {
    expect(component.survey.questions.length).toBeGreaterThan(0, 'question list is not empty');

    component.pager.count = 1;
    component.pager.index = 0;
    let sentQuestion = component.survey.questions[component.pager.index];
    sentQuestion.options[component.pager.index].selected = true;

    const parentRetVal = component.canReview(component.pager.index);
    expect(parentRetVal).toEqual(true);
  });

  it('should \'onSubmit\' have been called once', () => {
    component.onSubmit();
    expect(component.onSubmit.call.length).toBe(1, 'get called once');
  });

  it('should \'onSubmit\' fill the component answers', () => {
    expect(component.answers).toEqual(new SurveyPostResult({ ResultJson: "" }));

    component.pager.index = 0;
    let question = component.survey.questions[component.pager.index];
    question.options[0].selected = true;

    component.survey.reviewQuestions.push(question);
    component.onSubmit();

    expect(component.answers).not.toEqual(new SurveyPostResult({ ResultJson: "" }));
  });

  it('should \'previous\' have been called once', () => {
    component.previous(component.pager.index);
    expect(component.previous.call.length).toBe(1, 'get called once');
  });

  it('should \'previous\' clean the answer of last question', () => {
    component.pager.index = 0;
    let question = component.survey.questions[component.pager.index + 1];
    question.options[0].selected = true;
    expect(question.options.filter(x => x.selected == true).length).not.toEqual(0);

    component.previous(component.pager.index);
    expect(question.options.filter(x => x.selected == true).length).toEqual(0);
  });

  it('should \'previous\' give the correct result when the current question has a parent and is displayed with the correct option.', () => {
    component.pager.index = component.survey.questions.indexOf(component.survey.questions.filter(x => x.id == 3)[0]);

    // The question with id == 3 is retrieved from testSurvey list.
    let filteredQuestion = component.survey.questions[component.pager.index];
    expect(filteredQuestion).toBeTruthy();

    // Parent question is retrieved.
    let parentQuestion = component.survey.questions.filter(x => x.id == filteredQuestion.parent.parentId);
    expect(parentQuestion.length).toEqual(1);

    // Second option is selected of parent question.
    parentQuestion[0].options[1].selected = true;

    component.previous(component.pager.index - 1);

    expect(parentQuestion[0].options[1].id).toEqual(filteredQuestion.parent.optionId);
  });

  it('should \'previous\' give the incorrect result when question.parent.option is not equal to the selected option of parent question.', () => {
    component.pager.index = component.survey.questions.indexOf(component.survey.questions.filter(x => x.id == 3)[0]);

    // The question with id == 3 is retrieved from mock list (testSurvey).
    let filteredQuestion = component.survey.questions[component.pager.index];
    expect(filteredQuestion).toBeTruthy();

    // Parent question is retrieved.
    let parentQuestion = component.survey.questions.filter(x => x.id == filteredQuestion.parent.parentId);
    expect(parentQuestion.length).toEqual(1);

    // First option is selected of parent question.
    parentQuestion[0].options[0].selected = true;

    component.previous(component.pager.index - 1);

    expect(parentQuestion[0].options[0].id).not.toEqual(filteredQuestion.parent.optionId);
  });

  it('should \'previous\' give the correct result when question has no parent', () => {
    let filteredQuestions = component.survey.questions.filter(x => !x.parent.parentId);
    expect(filteredQuestions.length).toBeGreaterThan(0);

    const lastFilteredQuestionIndex = filteredQuestions[filteredQuestions.length - 1].id - 1;
    component.pager.index = lastFilteredQuestionIndex;
    component.previous(component.pager.index - 1);

    expect(component.pager.index).toEqual(lastFilteredQuestionIndex - 1);
  });

  it('should \'next\' have been called once', () => {
    component.next(component.pager.index);
    expect(component.next.call.length).toBe(1, 'get called once');
  });

  it('should \'next\' give the correct result when next question has no parent and not answered.', () => {
    component.pager.index = component.survey.questions.indexOf(component.survey.questions.filter(x => x.id == 1)[0]);

    let filteredQuestion = component.survey.questions[component.pager.index + 1];
    expect(filteredQuestion).toBeTruthy();

    component.next(component.pager.index + 1);

    expect(component.pager.index).not.toEqual(component.pager.index + 1);
  });

  it('should \'next\' give the correct result when next question has no parent and answered.', () => {
    component.pager.index = component.survey.questions.indexOf(component.survey.questions.filter(x => x.id == 1)[0]);

    let filteredQuestion = component.survey.questions[component.pager.index];
    expect(filteredQuestion).toBeTruthy();

    if (filteredQuestion.questionTypeId == 1) {
      filteredQuestion.options[0].selected = true;
    } else if (filteredQuestion.questionTypeId == 2) {
      filteredQuestion.value = 'test';
    }

    const nextIndex = component.pager.index + 1;
    component.next(nextIndex);

    expect(nextIndex).toEqual(component.pager.index);
  });

  it('should \'next\' return the correct result when any of the subsequent questions have a parent as the valid question and the option selected is correct.', () => {
    component.pager.index = component.survey.questions.indexOf(component.survey.questions.filter(x => x.id == 1)[0]);

    let currentQuestion = component.survey.questions[component.pager.index];
    expect(currentQuestion).toBeTruthy();

    currentQuestion.options[0].selected = true;
    const currentIndex = component.pager.index;

    component.next(currentIndex + 1);
    expect(currentIndex).not.toEqual(component.pager.index);

    // Parent question is retrieved.
    let nextQuestion = component.survey.questions[component.pager.index];

    // The parent id of the next question is checked with the current question's selected option.
    expect(nextQuestion.parent.parentId).toEqual(currentQuestion.options.filter(x => x.selected == true)[0].id);
  });

  // Helper method.
  async function createComponent() {
    fixture = TestBed.createComponent(SurveyComponent);
    component = fixture.componentInstance;

    // 1st change detection triggers ngOnInit which gets a survey
    fixture.detectChanges();
    await fixture.whenStable();
    // 2nd change detection displays the async-fetched survey
    fixture.detectChanges();
    surveyDe = fixture.debugElement;
  }
}