import { Survey } from "../models";
import { ParentRelation } from "../models/parentRelation";

export function getTestSurvey(): Survey {
  return ({
        id: 1,
        name: 'Survey',
        description: 'Description.',
        config: { allowBack: true },
        reviewQuestions: [],
        questions: [{
          id: 1,
          name: 'Question-1',
          questionTypeId: 1,
          parent: new ParentRelation({}),
          value: '',
          multiSelect: false,
          answered: false,
          options: [{
            id: 1,
            name: "Answer-1",
            selected: true
          },
          {
            id: 2,
            name: "Answer-2",
            selected: false
          }]
        },
        {
          id: 2,
          name: 'Question-2',
          questionTypeId: 1,
          parent: new ParentRelation({ parentId: 1, optionId: 1 }),
          value: '',
          multiSelect: false,
          answered: false,
          options: [{
            id: 1,
            name: "Answer-1",
            selected: true
          },
          {
            id: 2,
            name: "Answer-2",
            selected: false
          }]
        }, {
          id: 3,
          name: 'Question-3',
          questionTypeId: 1,
          parent: new ParentRelation({ parentId: 1, optionId: 2 }),
          value: '',
          multiSelect: true,
          answered: false,
          options: [
            {
              id: 1,
              name: "Answer-1",
              selected: false
            },
            {
              id: 2,
              name: "Answer-2",
              selected: false
            },
            {
              id: 3,
              name: "Answer-3",
              selected: false
            }
          ]
        }, {
          id: 4,
          name: 'Question-4',
          questionTypeId: 2,
          parent: new ParentRelation({}),
          value: '',
          multiSelect: false,
          answered: false,
          options: []
        },
        {
          id: 5,
          name: 'Question-5',
          questionTypeId: 5,
          parent: new ParentRelation({}),
          value: '',
          multiSelect: false,
          answered: false,
          options: []
        }]
      }
    );
}