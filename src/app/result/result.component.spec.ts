import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ResultComponent } from './result.component';
import { DebugElement } from '@angular/core';

let component: ResultComponent;
let fixture: ComponentFixture<ResultComponent>;
let surveyDe: DebugElement;

describe('ResultComponent', () => {
  describe('when override the SurveyService provided', overrideSetup);
});

function overrideSetup() {
  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        providers: []
      })
      .compileComponents();
  });

  beforeEach(async () => {
    await createComponent();
  });

  it('should \'h2\' element contain \'Thank\'', () => {
    expect(surveyDe.nativeElement.querySelector('h2').textContent).toContain('Thank');
  });

  // Helper method.
  async function createComponent() {
    fixture = TestBed.createComponent(ResultComponent);
    component = fixture.componentInstance;

    // 1st change detection triggers ngOnInit which gets a survey
    fixture.detectChanges();
    await fixture.whenStable();
    // 2nd change detection displays the async-fetched survey
    fixture.detectChanges();
    surveyDe = fixture.debugElement;
  }
}