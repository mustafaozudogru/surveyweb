import { Component } from '@angular/core';

@Component({
  template: '<h2 class="text-center font-weight-normal">Thank You.</h2>',
})

export class ResultComponent {}
