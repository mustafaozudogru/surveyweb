import { Component } from '@angular/core';

@Component({
  template: '<h2 class="text-center font-weight-normal">You have already completed the test.</h2>',
})

export class AlreadyAnsweredComponent {}
