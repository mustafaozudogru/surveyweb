import { ComponentFixture, TestBed } from '@angular/core/testing';
import { DebugElement } from '@angular/core';
import { ErrorComponent } from './error.component';

let component: ErrorComponent;
let fixture: ComponentFixture<ErrorComponent>;
let surveyDe: DebugElement;

describe('ErrorComponent', () => {
  describe('when override the SurveyService provided', overrideSetup);
});

function overrideSetup() {
  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        providers: []
      })
      .compileComponents();
  });

  beforeEach(async () => {
    await createComponent();
  });

  it('should \'h2\' element contain \'error\'', () => {
    expect(surveyDe.nativeElement.querySelector('h2').textContent).toContain('error');
  });

  // Helper method.
  async function createComponent() {
    fixture = TestBed.createComponent(ErrorComponent);
    component = fixture.componentInstance;

    // 1st change detection triggers ngOnInit which gets a survey
    fixture.detectChanges();
    await fixture.whenStable();
    // 2nd change detection displays the async-fetched survey
    fixture.detectChanges();
    surveyDe = fixture.debugElement;
  }
}