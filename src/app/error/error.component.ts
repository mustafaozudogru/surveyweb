import { Component } from '@angular/core';

@Component({
  template: '<h2 class="text-center font-weight-normal">An error was received during the operation.</h2>',
})

export class ErrorComponent {}
