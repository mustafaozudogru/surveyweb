import { ComponentFixture, TestBed } from '@angular/core/testing';
import { AlreadyAnsweredComponent } from './alreadyAnswered.component';
import { DebugElement } from '@angular/core';

let component: AlreadyAnsweredComponent;
let fixture: ComponentFixture<AlreadyAnsweredComponent>;
let surveyDe: DebugElement;

describe('AlreadyAnsweredComponent', () => {
  describe('when override the SurveyService provided', overrideSetup);
});

function overrideSetup() {
  beforeEach(async () => {
    await TestBed
      .configureTestingModule({
        providers: []
      })
      .compileComponents();
  });

  beforeEach(async () => {
    await createComponent();
  });

  it('should \'h2\' element contain \'already\'', () => {
    expect(surveyDe.nativeElement.querySelector('h2').textContent).toContain('already');
  });

  // Helper method.
  async function createComponent() {
    fixture = TestBed.createComponent(AlreadyAnsweredComponent);
    component = fixture.componentInstance;

    // 1st change detection triggers ngOnInit which gets a survey
    fixture.detectChanges();
    await fixture.whenStable();
    // 2nd change detection displays the async-fetched survey
    fixture.detectChanges();
    surveyDe = fixture.debugElement;
  }
}