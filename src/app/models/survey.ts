import { SurveyConfig } from './survey-config';
import { Question } from './question';

export class Survey {
    id!: number;
    name!: string;
    description!: string;
    config!: SurveyConfig;
    questions!: Question[];
    reviewQuestions!: Question[];

    constructor(data: any) {
        if (data) {
            this.id = data.id;
            this.name = data.name;
            this.description = data.description;
            this.config = new SurveyConfig(data.config);
            this.questions = [];
            this.reviewQuestions = [];
            data.questions.forEach((q: any) => {
                this.questions.push(new Question(q));
            });
        }
    }
}
