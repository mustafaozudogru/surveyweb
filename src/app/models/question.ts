import { Option } from './option';
import { ParentRelation } from './parentRelation';

export class Question {
    id: number;
    name: string;
    questionTypeId: number;
    parent: ParentRelation;
    options: Option[];
    value!: string;
    multiSelect: boolean;
    answered!: boolean;

    constructor(data: any) {
        data = data || {};
        this.id = data.id;
        this.name = data.name;
        this.questionTypeId = data.questionTypeId;
        this.parent = new ParentRelation(data.parent);
        this.options = [];
        this.value = data.value || '';
        this.multiSelect = data.multiSelect;
        data.options.forEach((o: any) => {
            this.options.push(new Option(o));
        });
    }
}
