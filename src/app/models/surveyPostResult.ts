export class SurveyPostResult {
    ResultJson: string;

    constructor(data: any) {
        data = data || {};
        this.ResultJson = data.ResultJson;
    }
}
