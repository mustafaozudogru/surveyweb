export class ParentRelation {
    parentId: number;
    optionId: number;

    constructor(data: any) {
        data = data || {};
        this.parentId = data.parentId;
        this.optionId = data.optionId;
    }
}
